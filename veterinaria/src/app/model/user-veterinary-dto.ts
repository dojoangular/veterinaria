export class UserVeterinaryDto {
  idUser?: Number;
  name?: String;
  lastName?: String;
  documentType?: String;
  documentNumber?: String;
  birthDate?: Date;
  department?: String;
  city?: String;
  neighborhood?: String;
  phone?: Number;
  email?: String;
  userName?: String;
  pass?: String;
  status?: Boolean;
  rol?: Number;
  token?:String;


}
