import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthenticationRoutingModule } from './authentication-routing.module';
import { CardModule } from 'primeng/card';
import { InputTextModule } from 'primeng/inputtext';
import { InputNumberModule } from 'primeng/inputnumber';
import { ReactiveFormsModule } from '@angular/forms';
import { MenubarModule } from 'primeng/menubar';
import { ToastModule } from 'primeng/toast';
import { PasswordModule } from 'primeng/password';
import { ButtonModule } from 'primeng/button';

import { AuthenticationComponent } from './authentication.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';

import { AuthServiceService } from '../../core/services/auth-service.service';

@NgModule({
  declarations: [AuthenticationComponent, SignInComponent, SignUpComponent],
  imports: [CommonModule,
    AuthenticationRoutingModule,
    CardModule,
    InputTextModule,
    InputNumberModule,
    ReactiveFormsModule,
    MenubarModule,
    ToastModule,
    PasswordModule,
    ButtonModule
  ],
  providers: [AuthServiceService],
})
export class AuthenticationModule {}
