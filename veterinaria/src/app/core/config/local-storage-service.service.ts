import { Injectable } from '@angular/core';

export function getKey() : String{
  const key:any = localStorage.getItem('token')
  return atob(key);
}

@Injectable({
  providedIn: 'root'
})
export class LocalStorageServiceService {

  constructor() { }

  getItem(key: string) {
    return localStorage.getItem(key);
  }

  setKey(key :string, data: any) {
    localStorage.setItem(key, data);
  }

  clear(){
    localStorage.clear();
  }

}
