import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import Swal from 'sweetalert2';

@Injectable()
export class HttpServiceService<T> {
  constructor(
    public httpClient: HttpClient,
    public url: String,
    public router: Router
  ) {}

  public changeUrl(newUrl: String) {
    this.url = newUrl;
  }

  public get(endpoint: string, params?: {}): Observable<T[]> {
    return this.httpClient.get<T[]>(`${this.url}/${endpoint}`, params).pipe(
      map((response: any) => {
        if (response.status == 500) {
          throw new Error(JSON.stringify(response.payload));
        }
        if (response.status == 200) {
          return response.payload;
        }
      }),
      catchError( (error) => {
        return this.errorHttp(error)
      })
    );
  }

  public post(endpoint: string, body: T): Observable<any> {
  return this.httpClient.post<T[]>(`${this.url}/${endpoint}`, body).pipe(
    map((response: any) => {
      if (response.status == 500) {
        throw new Error(JSON.stringify(response.payload));
      }
      if (response.status == 200) {
        return response.payload;
      }
    }),
    catchError( error => {
      return this.errorHttp(error)
    })

  );

}

public delete(endpoint: string, params?: {}): Observable<T[]> {
  return this.httpClient.delete<T[]>(`${this.url}/${endpoint}`, params).pipe(
    map((response: any) => {
      if (response.status == 500) {
        throw new Error(JSON.stringify(response.payload));
      }
      if (response.status == 200) {
        return response.payload;
      }
    }),
    catchError( (error) => {
      return this.errorHttp(error)
    })
  );
}

public put(endpoint: string, body: T): Observable<any> {
  return this.httpClient.put<T[]>(`${this.url}/${endpoint}`, body).pipe(
    map((response: any) => {
      if (response.status == 500) {
        throw new Error(JSON.stringify(response.payload));
      }
      if (response.status == 200) {
        return response.payload;
      }
    }),
    catchError( error => {
      return this.errorHttp(error)
    })

  );

}

private errorHttp(error: any) : Observable<any>{
  if (error instanceof HttpErrorResponse) {
    Swal.fire({
      icon: 'error',
      title: 'Game over',
      text: 'Falló la conexión con el servidor',
    });
    return throwError('Falló la conexión con el servidor');
  } else {
    return throwError(error.message);
  }

}

}
