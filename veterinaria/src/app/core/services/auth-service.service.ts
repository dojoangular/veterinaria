import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { LoginRequestDto } from 'src/app/model/login-request-dto';
import { UserVeterinaryDto } from 'src/app/model/user-veterinary-dto';
import { environment } from 'src/environments/environment';
import { HttpServiceService } from '../config/http-service.service';
import { LocalStorageServiceService } from '../config/local-storage-service.service';

@Injectable({
  providedIn: 'root',
})
export class AuthServiceService extends HttpServiceService<UserVeterinaryDto> {
  constructor(
    public httpClient: HttpClient,
    private localStorage: LocalStorageServiceService,
    public router: Router
  ) {
    super(httpClient, environment.BaseUrl + '/auth', router);
  }

  signUp(user:UserVeterinaryDto): Observable<any> {
    return super.post('/register', user);
  }

  singIn(login:LoginRequestDto): Observable<any> {
    return super.post('/login', login);
  }
}
